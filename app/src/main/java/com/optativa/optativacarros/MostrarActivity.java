package com.optativa.optativacarros;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.optativa.optativacarros.BaseDeDatosSqlite.Carro;
import com.optativa.optativacarros.BaseDeDatosSqlite.OpenHelper;

import java.util.ArrayList;

public class MostrarActivity extends AppCompatActivity {

    ListView listViewCarros;
    ArrayList<Carro> listaCarros;
    ArrayList<String> listaInformacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);

        listViewCarros = findViewById(R.id.listViewCarros);

        consultarCarro();

        ArrayAdapter adaptador = new ArrayAdapter(this,
                R.layout.support_simple_spinner_dropdown_item, listaInformacion);

        listViewCarros.setAdapter(adaptador);

        listViewCarros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final CharSequence[] opciones={"Modificar","Eliminar","Cancelar"};
                AlertDialog.Builder alertOpciones = new AlertDialog.Builder(MostrarActivity.this);
                alertOpciones.setTitle("Seleccione una opcion");
                alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (opciones[i].equals("Modificar")){
                            Intent intent = new Intent(MostrarActivity.this, ModificarActivity.class);
                            intent.putExtra("id", listaCarros.get(position).getId());
                            intent.putExtra("placa", listaCarros.get(position).getPlaca());
                            intent.putExtra("marca", listaCarros.get(position).getMarca());
                            intent.putExtra("modelo", listaCarros.get(position).getModelo());
                            intent.putExtra("anyo", listaCarros.get(position).getAnyo());
                            intent.putExtra("motor", listaCarros.get(position).getMotor());
                            intent.putExtra("combustible", listaCarros.get(position).getCombustible());
                            intent.putExtra("color", listaCarros.get(position).getColor());
                            intent.putExtra("direccion", listaCarros.get(position).getDireccion());
                            startActivity(intent);
                            finish();
                        }else if (opciones[i].equals("Eliminar")){
                            OpenHelper conexion = new OpenHelper(MostrarActivity.this, "Carro", null, 1);
                            SQLiteDatabase db = conexion.getWritableDatabase();
                            db.delete("Carro","id="+ listaCarros.get(position).getId(),null);
                            db.close();
                            Toast.makeText(MostrarActivity.this, "Se eliminó con éxito", Toast.LENGTH_SHORT).show();
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }else {
                            dialog.dismiss();
                        }
                    }
                });alertOpciones.show();
            }
        });



    }

    private void consultarCarro() {
        OpenHelper conexion = new OpenHelper(this, "Carro", null, 1);
        SQLiteDatabase db = conexion.getReadableDatabase();
        listaCarros = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from Carro", null);
        while (cursor.moveToNext()){
            Carro carro = new Carro();
            carro.setId(cursor.getInt(0));
            carro.setPlaca(cursor.getString(1));
            carro.setMarca(cursor.getString(2));
            carro.setModelo(cursor.getString(3));
            carro.setAnyo(cursor.getInt(4));
            carro.setMotor(cursor.getString(5));
            carro.setCombustible(cursor.getString(6));
            carro.setColor(cursor.getString(7));
            carro.setDireccion(cursor.getString(8));
            listaCarros.add(carro);
        }
        mostrarLista();
    }

    private void mostrarLista() {
        listaInformacion = new ArrayList<>();
        for (int i=0; i<listaCarros.size(); i++){
            listaInformacion.add("PLACA: " + listaCarros.get(i).getPlaca()  + " MARCA: " + listaCarros.get(i).getMarca());
        }

    }
}