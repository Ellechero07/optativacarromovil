package com.optativa.optativacarros;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.optativa.optativacarros.BaseDeDatosSqlite.OpenHelper;

public class ModificarActivity extends AppCompatActivity {

    EditText txtModPlaca, txtModMarca, txtModModelo, txtModAnyo, txtModMotor, txtModCombustible, txtModColor, txtModDireccion;
    TextView lblModId;
    Button btnModificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);

        lblModId = findViewById(R.id.lblModificarId);
        txtModPlaca = findViewById(R.id.txtModPlaca);
        txtModMarca = findViewById(R.id.txtModMarca);
        txtModModelo = findViewById(R.id.txtModModelo);
        txtModAnyo = findViewById(R.id.txtModAnyo);
        txtModMotor = findViewById(R.id.txtModMotor);
        txtModCombustible = findViewById(R.id.txtModCombustible);
        txtModColor = findViewById(R.id.txtModcolor);
        txtModDireccion = findViewById(R.id.txtModDireccion);
        btnModificar = findViewById(R.id.btnModificarCarro);

        final int id = getIntent().getIntExtra("id",0);
        lblModId.setText(String.valueOf(id));
        txtModPlaca.setText(getIntent().getStringExtra("placa"));
        txtModMarca.setText(getIntent().getStringExtra("marca"));
        txtModModelo.setText(getIntent().getStringExtra("modelo"));
        txtModAnyo.setText(String.valueOf(getIntent().getIntExtra("anyo", 0)));
        txtModMotor.setText(getIntent().getStringExtra("motor"));
        txtModCombustible.setText(getIntent().getStringExtra("combustible"));
        txtModColor.setText(getIntent().getStringExtra("color"));
        txtModDireccion.setText(getIntent().getStringExtra("direccion"));

        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Modificar(id);
            }

            private void Modificar(int id) {
                OpenHelper conexion = new OpenHelper(getApplicationContext(), "Carro", null, 1);
                SQLiteDatabase db = conexion.getWritableDatabase();
                String placa = txtModPlaca.getText().toString();
                String marca = txtModMarca.getText().toString();
                String modelo = txtModModelo.getText().toString();
                String anyo = txtModAnyo.getText().toString();
                String motor = txtModMotor.getText().toString();
                String combustible = txtModCombustible.getText().toString();
                String color = txtModColor.getText().toString();
                String direccion = txtModDireccion.getText().toString();
                if (!placa.isEmpty() && !marca.isEmpty()
                        && !modelo.isEmpty() && !anyo.isEmpty() && !motor.isEmpty()
                        && !combustible.isEmpty() && !color.isEmpty() && !direccion.isEmpty()){
                    ContentValues modificar = new ContentValues();
                    modificar.put("placa", placa);
                    modificar.put("marca", marca);
                    modificar.put("modelo", modelo);
                    modificar.put("anyo", anyo);
                    modificar.put("motor", motor);
                    modificar.put("combustible", combustible);
                    modificar.put("color", color);
                    modificar.put("direccion", direccion);
                    db.update("Carro", modificar, "id="+id,null);
                    Toast.makeText(getApplicationContext(), "Se modificó correctamente el carro", Toast.LENGTH_SHORT).show();
                    db.close();
                    startActivity(new Intent(getApplicationContext(), MostrarActivity.class));
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(), "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}