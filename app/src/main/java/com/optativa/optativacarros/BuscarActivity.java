package com.optativa.optativacarros;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.optativa.optativacarros.BaseDeDatosSqlite.OpenHelper;

public class BuscarActivity extends AppCompatActivity {

    EditText txtBuscarPlaca;
    Button btnBuscar;
    TextView lblPlaca, lblMarca, lblModelo, lblAnyo, lblMotor, lblCombustible, lblColor, lblDireccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);

        txtBuscarPlaca = findViewById(R.id.txtBuscarPlaca);
        lblPlaca = findViewById(R.id.lblPlaca);
        lblMarca = findViewById(R.id.lblMarca);
        lblModelo = findViewById(R.id.lblModelo);
        lblAnyo = findViewById(R.id.lblAnyo);
        lblMotor = findViewById(R.id.lblMotor);
        lblCombustible = findViewById(R.id.lblCombustible);
        lblColor = findViewById(R.id.lblColor);
        lblDireccion = findViewById(R.id.lblDireccion);

        btnBuscar = (Button) findViewById(R.id.btnBusqueda);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String id = txtBuscarPlaca.getText().toString();
                    OpenHelper conexion = new OpenHelper(getApplicationContext(), "Carro", null, 1);
                    SQLiteDatabase db = conexion.getReadableDatabase();
                    Cursor fila = db.rawQuery("select placa, marca, modelo, anyo, motor, combustible, color, direccion from Carro where id=" + id,null);
                    if (fila.moveToFirst()){
                        lblPlaca.setText("PLACA: "+ fila.getString(0));
                        lblMarca.setText("MARCA: " + fila.getString(1));
                        lblModelo.setText("MODELO: " + fila.getString(2));
                        lblAnyo.setText("AÑO: " + fila.getInt(3));
                        lblMotor.setText("MOTOR: " + fila.getString(4));
                        lblCombustible.setText("COMBUSTIBLE: " + fila.getString(5));
                        lblColor.setText("COLOR: " + fila.getString(6));
                        lblDireccion.setText("DIRECCION: " + fila.getString(7));
                        db.close();
                    }else {
                        Toast.makeText(getApplicationContext(), "No existe el CARRO", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.e("error", e.getMessage());
                }
            }
        });
    }
}