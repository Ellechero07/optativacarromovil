package com.optativa.optativacarros;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.optativa.optativacarros.BaseDeDatosSqlite.OpenHelper;

public class AgregarActivity extends AppCompatActivity {

    EditText txtPlaca, txtMarca, txtModelo, txtAnyo, txtMotor, txtCombustible, txtColor, txtDireccion;
    Button btnAgregar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);
        txtPlaca = findViewById(R.id.txtPlaca);
        txtMarca = findViewById(R.id.txtMarca);
        txtModelo = findViewById(R.id.txtModelo);
        txtAnyo = findViewById(R.id.txtAnyo);
        txtMotor = findViewById(R.id.txtMotor);
        txtCombustible = findViewById(R.id.txtCombustible);
        txtColor = findViewById(R.id.txtColor);
        txtDireccion = findViewById(R.id.txtDireccion);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Agregar();
            }
        });


    }

    private void Agregar() {

        //Establecer conexion con la clase OpenHelper para acceder a la base de datos
        OpenHelper conexion = new OpenHelper(this, "Carro", null, 1);
        //Pone a la base de datos en modo de escritura
        SQLiteDatabase db = conexion.getWritableDatabase();

        //Asigna variables de tipo String a los campos de textos
        String placa = txtPlaca.getText().toString();
        String marca = txtMarca.getText().toString();
        String modelo = txtModelo.getText().toString();
        String anyo = txtAnyo.getText().toString();
        String motor = txtMotor.getText().toString();
        String combustible = txtCombustible.getText().toString();
        String color = txtColor.getText().toString();
        String direccion = txtDireccion.getText().toString();


        //Validacion de campos
        if (!placa.isEmpty() && !marca.isEmpty()
                && !modelo.isEmpty() && !anyo.isEmpty() && !motor.isEmpty()
                && !combustible.isEmpty() && !color.isEmpty() && !direccion.isEmpty() ){

            //Clase ContentValues para guardar los valores
            ContentValues valores = new ContentValues();
            //Metodo put para agregar al objeto ContentValues
            valores.put("placa", placa);
            valores.put("marca", marca);
            valores.put("modelo", modelo);
            valores.put("anyo", anyo);
            valores.put("motor", motor);
            valores.put("combustible", combustible);
            valores.put("color", color);
            valores.put("direccion", direccion);

            //Inserta a la tabla Carro los datos que se guardan en el objeto ContentValues

            db.insert("Carro", null, valores);
            Toast.makeText(this, "Se agregó con exito", Toast.LENGTH_SHORT).show();
            //Se cierra la base de datos
            db.close();
            txtPlaca.setText("");
            txtMarca.setText("");
            txtModelo.setText("");
            txtAnyo.setText("");
            txtMotor.setText("");
            txtCombustible.setText("");
            txtColor.setText("");
            txtDireccion.setText("");

        } else {
            Toast.makeText(this, "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show();
        }

    }

}