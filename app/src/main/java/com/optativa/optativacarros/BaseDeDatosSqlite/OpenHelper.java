package com.optativa.optativacarros.BaseDeDatosSqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class OpenHelper extends SQLiteOpenHelper {

    public OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Sentencia SQL que crea la tabla Carro
        db.execSQL("create table Carro(id INTEGER PRIMARY KEY AUTOINCREMENT, placa text, marca text, modelo text, anyo int, motor text, combustible text, color text, direccion text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists Carro");
        onCreate(db);
    }
}
